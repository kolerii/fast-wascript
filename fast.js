DEVELOPMENT_MODE = true;

URL_REMOTE_LOG_COLLECTOR = 'http://example.com/collector.php';
URL_REMOTE_LOG_FILENAME = 'my_log_file';
URL_REMOTE_LOG_PASSWORD = 'my_test';

global_dom_idx = 0;
_SESSION_UID = Math.floor((Math.random() * 10000000)+1);

function get_next_idx()
{
	return global_dom_idx++;
}

Object.prototype.keys = function ()
{
  var keys = [];
  for(var i in this) if (this.hasOwnProperty(i))
  {
    keys.push(i);
  }
  return keys;
}
Object.prototype.values = function ()
{
  var keys = [];
  for(var i in this) if (this.hasOwnProperty(i))
  {
    keys.push(this[i]);
  }
  return keys;
}

function _checkElement( obj )
{
	var rect = getElementRect(obj);
	return !(rect['left'] == 0 && rect['top'] == 0 && rect['right'] == 0 && rect['bottom'] == 0);
}

function createTimer( elapse_time ){
	return {
		start_time : new Date(),
		elapse : elapse_time,
		check : function(){
			var current_time = new Date();
			return parseInt((current_time-this.start_time)/1000) >= this.elapse ? true : false;
		},
		reset : function(){
			this.start_time = new Date();
		}
	}
}

Fast = {
	getElement : function(selector, idx){
		var el = jQueryGetElement( selector , idx == undefined ? 0 : idx );
		if(el.varName)
			return this._createWrapperElement(el);
		return false;
	},
	getInternalLink : function(idx){
		var el = getInternalLink( idx == undefined ? 0 : idx);
		if(el.varName)
			return this._createWrapperElement(el);
		return false;
	},
	getElements : function( selector ){
		var els = jQueryGetElements(selector);
		if(els.varName)
		{
			var start_idx = get_next_idx();
			executeJavaScript('var start_idx = '+start_idx+';for(var i = 0; i < '+els.length+';i++){window["wacs_found_element_80000_"+(start_idx+i)] = '+els.varName+'[i];}');

			var arr = [];
			for(var i = 0; i < els.length; i++){
				arr.push(this._createWrapperElement({'varName':'wacs_found_element_80000_'+(i+start_idx), 'frameName':''}));
			}
			return arr;
		}
		return false;
	},
	log : function( message, ended_success )
	{
	    log(message);
	    if(DEVELOPMENT_MODE)
	    {
		    if( ended_success === true )
		        message += '$end';
		    if( ended_success === false )
		        message += '$fail';
		    postForm(URL_REMOTE_LOG_COLLECTOR, ['filename','password','message','uid'], [URL_REMOTE_LOG_FILENAME, URL_REMOTE_LOG_PASSWORD, message, _SESSION_UID])
	    }
	},
	setUserAgent : function( useragent ){
		setUserAgent(useragent);
	},
	useragent : function(){
		executeJavaScript('$("body").attr("ua",navigator.userAgent);');
		var e = this.getElement('body');
		if(e){
			var useragent = e.attr('ua');
			e.removeAttr('ua');
			return useragent;
		}
		return false;
	},
	get : function( url, referrer ){
		loadURI(url, referrer);
	},
	url : function(){
		var loc = getLocation(),
            url = loc['protocol'] + '://' + loc['host'] + loc['path'] + '?' +loc['params'];
        return url;
	},
	msleep : function(msec){
		wait(msec);
	},
	post : function( url, args ){
		postForm(url, args.keys(), args.values());
	},
	wait : function(max, min){
		waitLoaded(max === undefined ? 7000 : max, min == undefined ? 1000 : min);
	},
	_createWrapperElement : function(object){
		if(!_checkElement(object)) return false;
		return {
			_object : object,
			click : function( x, y ){
				return clickElement(this._object, x === undefined ? 0 : x, y === undefined ? 0 : y);
			},
			clickJS : function(){executeJavaScript('$('+this._object.varName+').click();')},
			write : function( text ){
				if(this.click())
				{
					typeIn(text);
					return true;
				}
				return false;
			},
			_struct : function(){
				var newElement = {'varName':'wacs_found_element_80000_' + get_next_idx()};
		    	newElement.frameName = this._object.frameName;
		    	return newElement;
			},
			parent : function(){
				var newElement = this._struct();
				executeJavaScript(newElement.varName + ' = ' + this._object.varName + '.parentNode', this._object.frameName);
				return Fast._createWrapperElement(newElement);
			},
			prevSibling : function(){
			    var newElement = this._struct();
				executeJavaScript(newElement.varName + ' = ' + this._object.varName + '.previousElementSibling', this._object.frameName);
				return Fast._createWrapperElement(newElement);
			},
			nextSibling : function(){
				var newElement = this._struct();
				executeJavaScript(newElement.varName + ' = ' + this._object.varName + '.nextElementSibling', this._object.frameName);
				return Fast._createWrapperElement(newElement);
			},
			firstChild : function(){
				var newElement = this._struct();
				executeJavaScript(newElement.varName + ' = ' + this._object.varName + '.firstChild', this._object.frameName);
				return Fast._createWrapperElement(newElement);
			},
			lastChild : function(){
				var newElement = this._struct();
				executeJavaScript(newElement.varName + ' = ' + this._object.varName + '.lastChild', this._object.frameName);
				return Fast._createWrapperElement(newElement);
			},
			prev : function(){return this.prevSibling()},
			next : function(){return this.nextSibling()},
			attribute : function(name){
				return getElementAttribute(this._object, name);
			},
			attr : function(name){return this.attribute(name)},
			removeAttr : function(name){executeJavaScript('$('+this._object.varName+').removeAttr();')},
			removeElement : function(){executeJavaScript('$('+this._object.varName+').remove();')},
			remove : function(){this.removeElement()},
			move : function(){moveToElement(this._object);},
			innertext : function(){return this.attr('innerText');},
			focus : function(){executeJavaScript('$('+this._object.varName+').focus();')}
		};
	}
}